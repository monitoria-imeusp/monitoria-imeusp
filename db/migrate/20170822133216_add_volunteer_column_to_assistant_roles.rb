class AddVolunteerColumnToAssistantRoles < ActiveRecord::Migration
  def change
    add_column :assistant_roles, :volunteer, :boolean, default: false, null: false
  end
end
