class UpdatePastSemestersBeginEndDate < ActiveRecord::Migration
  def change
  	Semester.all.each do |semester|
  		if semester.parity == 0
  			semester.update(started_at: Time.new(semester.year, 3, 1))
  			semester.update(finished_at: Time.new(semester.year, 6, 30))		
  		else
  			semester.update(started_at: Time.new(semester.year, 8, 1))
  			semester.update(finished_at: Time.new(semester.year, 11, 30))		
  		end
    end
  end
end
