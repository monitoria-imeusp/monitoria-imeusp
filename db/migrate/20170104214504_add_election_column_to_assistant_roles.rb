class AddElectionColumnToAssistantRoles < ActiveRecord::Migration
  def change
    add_column :assistant_roles, :election, :integer, default: 1, null: false
  end
end
