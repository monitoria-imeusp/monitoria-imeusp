FROM ubuntu:16.04
MAINTAINER MonitoriaIME

#Setting up envoirement
RUN apt-get update -qq
RUN apt-get install -yqq build-essential chrpath libssl-dev libxft-dev libfreetype6 libfreetype6-dev libfontconfig1 libfontconfig1-dev
RUN apt-get install -yqq curl libqt4-dev libqtwebkit-dev postgresql postgresql-server-dev-all sqlite3 libsqlite3-dev sed grep tar curl wget monit git

#Installing phantomJS
WORKDIR /root
RUN wget https://www.ime.usp.br/~kazuo/monitoria/phantomjs-1.9.7-linux-x86_64.tar.bz2
RUN mv phantomjs-1.9.7-linux-x86_64.tar.bz2 /usr/local/share/
WORKDIR /usr/local/share/
RUN tar xjf phantomjs-1.9.7-linux-x86_64.tar.bz2
RUN mv phantomjs-1.9.7-linux-x86_64 phantomjs-1.9.7-linux-i686
RUN ln -s /usr/local/share/phantomjs-1.9.7-linux-i686 /usr/local/share/phantomjs
RUN ln -s /usr/local/share/phantomjs/bin/phantomjs /usr/local/bin/phantomjs
RUN phantomjs -v

#Installing ruby
WORKDIR /root
RUN wget http://ftp.ruby-lang.org/pub/ruby/2.3/ruby-2.3.1.tar.gz
RUN tar -xzvf ruby-2.3.1.tar.gz
WORKDIR /root/ruby-2.3.1/
RUN ls -l
RUN ./configure
RUN make
RUN make install
RUN ruby -v
RUN gem install bundler

#Installing firefox hedless
RUN apt-get update -qq
RUN apt-get install -yqq firefox xvfb
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.16.1/geckodriver-v0.16.1-linux64.tar.gz
RUN tar -xvf geckodriver-v0.16.1-linux64.tar.gz
RUN mv geckodriver /usr/bin/

EXPOSE 3000

WORKDIR /root