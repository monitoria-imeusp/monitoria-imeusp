class CloseFrequencyPeriodJob

  @semester
  @period

  def initialize semester, period
    @semester = semester
    @period = period
  end

  def perform
    @semester.close_frequency_period @period
  end

  def max_run_time
    30.minutes
  end

  def destroy_failed_jobs?
    false
  end

  def queue_name
    #FIXME: maybe change this?
    'pending_frequencies_queue'
  end

end
