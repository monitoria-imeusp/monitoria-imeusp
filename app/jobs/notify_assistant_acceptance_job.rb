
class NotifyAssistantAcceptanceJob

  @semester

  def initialize semester
    @semester = semester
  end

	def perform
    AssistantRole.mail_accepted_assistants @semester
	end

	def max_run_time
		8.hours
	end

	def destroy_failed_jobs?
    false
	end

  def max_attempts
    1
  end

	def queue_name
		'notify_acceptance_queue'
	end

end
