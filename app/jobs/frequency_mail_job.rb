
class FrequencyMailJob

	def perform
    AssistantFrequency.notify_frequency
	end

	def max_run_time
		8.hours
	end

	def destroy_failed_jobs?
    false
	end

  def max_attempts
    1
  end

	def queue_name
		'notify_frequencies_queue'
	end

end
