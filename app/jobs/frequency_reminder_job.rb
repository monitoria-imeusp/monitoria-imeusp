class FrequencyReminderJob

	def perform
		AssistantFrequency.notify_frequency_reminder
	end

	def max_run_time
		8.hours
	end

	def destroy_failed_jobs?
  	false
	end

  def max_attempts
    1
  end

	def queue_name
		'pending_frequencies_queue'
	end

end
