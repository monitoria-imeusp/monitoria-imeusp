class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :authentication_keys => [:nusp], :omniauth_providers => [:usp]

  include ActiveModel::Validations
  #include Devise::Controllers::Helpers

  validates :name , presence: true
  validates :nusp , presence: true, inclusion: { in: 0..100000000 }, uniqueness: true
  validates :email , presence: true
  has_one :student
  has_one :professor

  def student
    result = Student.where(user_id: id)
    if block_given? and result.any? and not professor?
      yield result.take
    else
      result.take
    end
  end

  def student?
    Student.where(user_id: id).any? and not professor?
  end

  def professor
    result = Professor.where(user_id: id)
    if block_given? and result.any?
      yield result.take
    else
      result.take
    end
  end

  def professor?
    Professor.where(user_id: id).any?
  end

  def incomplete?
    !professor? and !student?
  end

  def super_professor?
    query = Professor.where(user_id: id)
    query.any? and query.take.super_professor?
  end

  def hiper_professor?
    query = Professor.where(user_id: id)
    query.any? and query.take.hiper_professor?
  end

  def should_see_role role
    professor do |prof|
      if prof.super_professor? and role.course.dep_code == prof.dep_code
        # Membros da comissão de monitoria vêm monitores do seu departamento inteiro
        return true
      elsif role.request_for_teaching_assistant.professor == prof
        # Professores normais vêem apenas seus próprios monitores
        return true
      end
    end
    return false
  end

  def self.from_omniauth auth
    registered = where nusp: auth.info.nusp
    link = auth.info.link
    if registered.any?
      user = registered.take
      if link == :student or link == :teacher
        user.name = auth.info.name
        user.email = auth.info.email
        user.provider = auth.provider
        user.uid = auth.uid
        user.save
      end
      if link == :teacher and not user.professor?
        make_professor user
      end
      user
    else
      user = User.new(nusp: auth.info.nusp, name: auth.info.name, email: auth.info.email, password: "changeme!")
      user.confirmed_at = Time.now
      unless user.save
        raise user.errors.inspect
      end
      if link == :teacher
        make_professor user
      end
      user
    end
  end

  private

  def self.make_professor user
    prof = Professor.new(user_id: user.id, department: Department.find_by(:code => "MAC"), dirty: true)
    raise prof.errors.inspect unless prof.save
  end

 end
