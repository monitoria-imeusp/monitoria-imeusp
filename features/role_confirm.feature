Feature: Assistant roles confirmation
    In order to manage the election of assistants
    As a secretary
    I want to confirm pre-elected assistants

    Background:
        When there is an open semester "2014" "1"
        And there is a department with code "MAC"
        And there is a course with name "Introdução à Ciência da Computação" and code "MAC0110" and department "MAC"
        And there is a student with name "Bob" with nusp "123456" and email "aluno@usp.br"
        And there is a professor with name "Dude" and nusp "111111" and department "MAC" and email "prof@ime.usp.br"
        And there is a request for teaching assistant by professor "Dude" for the course "MAC0110"
        And there is a candidature by student "Bob" for course "MAC0110"
        And there is an unconfirmed assistant role for student "Bob" with professor "Dude" at course "MAC0110"

    Scenario: Secretary confirms assistant role
        Given I'm logged in as a secretary
        And I visit the assistant roles page
        And I should see "Bob"
        And I should see "Não definitivo"
        When I click the "Confirmar" link
        Then I should see "Bob"
        And I should see "Aguardando aceite"
